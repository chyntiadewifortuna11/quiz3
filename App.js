// import 'react-native-gesture-handler';
// import React, {Component} from 'react';
// import AsyncStorage from '@react-native-community/async-storage';
// import {SafeAreaProvider} from 'react-native-safe-area-context';
// import {NavigationContainer} from '@react-navigation/native';
// import {createStackNavigator} from '@react-navigation/stack';
// import EStyleSheet from 'react-native-extended-stylesheet';

// import Login from './src/login/index';
// import Home from './src/home/index';
// import Register from './src/register/index';

// class App extends Component {
//   constructor() {
//     super();
//     this.state = {
//       allowRenderNavigator: false,
//       isUserLoggedin: false
//     };
//     this.stackNavigator = createStackNavigator();
//     EStyleSheet.build();
//     this.checkLoginSession();
//   }
//   // Render
//   render() {
//     const Stack = this.stackNavigator;
//     const {allowRenderNavigator, isUserLoggedin} = this.state;
//     if (allowRenderNavigator) {
//       return (
//         <SafeAreaProvider>
//           <NavigationContainer>
//             <Stack.Navigator
//               initialRouteName={'Login'}>
//               <Stack.Screen name="Home" component={Home}/>
//               <Stack.Screen name="Login" component={Login}/>
//               <Stack.Screen name="Register" component={Register}/>
//             </Stack.Navigator>
//           </NavigationContainer>
//         </SafeAreaProvider>
//       )
//     }
//     return null
//   }
// }

// export default App

import React, { Component } from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import { createAppContainer, createStackNavigator } from "react-navigation";
class HomeScreen extends Component {
  render() {
    return (
      <View>
        <Text>Home Screen</Text>
        <Button 
         onPress={() => this.props.navigation.navigate('About')}
         title='Goto About'/>
        <Button
         onPress={() => this.props.navigation.goBack()}
         title='Back'/>
      </View>
    );
  }
}
class AboutScreen extends Component {
  render() {
    return (
      <View>
        <Text>About Screen</Text>
        <Button
         onPress={() => this.props.navigation.goBack()}
         title='Back'/>
      </View>
    );
  }
}
const stack = createStackNavigator({
  Home: { screen: HomeScreen },
  About: { screen: AboutScreen },
},{
  headerMode: 'none'
});
export default createAppContainer(stack);