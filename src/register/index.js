import React, {Component} from 'react';
import
{KeyboardAvoidingView, View, TouchableOpacity, Text, Platform, Image, ScrollView} from 'react-native';
import {moderateScale} from 'react-native-size-matters/extend';
import EStyleSheet from 'react-native-extended-stylesheet';


const styles = EStyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#211e1f'
	},
	inputContainer: {
		marginBottom: moderateScale(16)
	},
	formContainer: {
		flex: 1,
		justifyContent: 'center',
		paddingHorizontal: moderateScale(32)
	},
	formContainerCenter: {
		position: 'relative',
	},
	loginButton: {
		alignItems: 'center',
		justifyContent: 'center',
		height: moderateScale(35),
		backgroundColor: '#08b2a9',
		marginTop: moderateScale(15),
		borderRadius: moderateScale(5)
	},
	loginButtonText: {
		color: '#ffffff',
		fontWeight: "700"
	},
	logoContainer: {
		alignItems: 'center'
	},
	logoSize: {
		width: moderateScale(150),
		height: moderateScale(30),
	},
	registerText: {
		color: '#ffffff',
		fontWeight: 'bold',
		fontSize: moderateScale(20),
		marginTop: moderateScale(20),
		marginBottom: moderateScale(30)
	},
	checkboxLabelText: {
		color: '#ffffff'
	}
});

class Register extends Component {
	constructor() {
		super();
	}

	// Render
	render() {
    return null
	}
}

export default Register
