import React, {Component} from 'react';
import {
  SafeAreaView, KeyboardAvoidingView, View, TouchableOpacity, Text, Platform, Image
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {moderateScale} from 'react-native-size-matters/extend';
import StyleSheet from 'react-native-extended-stylesheet';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-spinkit';
import axios from 'axios';
import logo from '../../assets/icon/iqb_logo.png';
import TextInput from 'components/Input/TextInput';
import {validateFormField, isFormValidationPass, setStateAsynchronous} from 'utils';
import Location from 'utils/location';



class Login extends Component {
	constructor() {
		super();
	}

	// Render
	render() {
		const isFormValid = isFormValidationPass(this.state.formField);
		return (
			<SafeAreaView style={styles.container}>
				<KeyboardAvoidingView style={styles.container} behavior={Platform.OS === 'ios' ? 'padding' : null}>
					<View style={styles.formContainer}>
						<View style={styles.formContainerCenter}>
							<View style={styles.logoContainer}>
								<Image style={styles.logoSize} source={logo}/>
								<Text style={styles.vendorText}>VENDOR</Text>
							</View>
							<View style={{marginTop: moderateScale(50)}}>
								<TextInput
									id="username"
									placeholder="username"
									textContentType="username"
									containerStyle={styles.inputContainer}
									validate={value => validateFormField(null, value)}
									onChangeText={this.onChangeTextInputHandler.bind(this)} />
								<TextInput
									id="password"
									placeholder="password"
									textContentType="password"
									secureTextEntry={true}
									containerStyle={styles.inputContainer}
									validate={value => validateFormField(null, value)}
									onChangeText={this.onChangeTextInputHandler.bind(this)} />
								<TouchableOpacity activeOpacity={0.7} onPress={() => this.onPressNavigateForgotPassword()}>
									<Text style={styles.forgotPasswordText}>
										Forgotten your password?
									</Text>
								</TouchableOpacity>
								<View style={{ opacity: isFormValid || !this.state.signinLoading ? 1 : 0.5,
									paddingTop:moderateScale(15)}}>
									<TouchableOpacity
										activeOpacity={0.7}
										style={styles.loginButton}
										onPress={() => this.onPressLoginButton()}
										disabled={!isFormValid || this.state.signinLoading}>
										{this.state.signinLoading ? (
											<Spinner
												color="#FFFFFF"
												type="ThreeBounce"
												size={moderateScale(40)}
												style={{ bottom: Platform.OS === 'ios' ? moderateScale(4) : 0 }} />
										) : <Text style={styles.loginButtonText}>LOGIN</Text>}
									</TouchableOpacity>
								</View>
								<View>
									<TouchableOpacity
										style={styles.registerButton}
										onPress={() => this.onPressRegisterButton()}>
										<Text style={styles.loginButtonText}>REGISTER</Text>
									</TouchableOpacity>
								</View>
							</View>
						</View>
					</View>
				</KeyboardAvoidingView>
			</SafeAreaView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#211e1f'
	},
	inputContainer: {
		marginBottom: moderateScale(16),
	},
	formContainer: {
		flex: 1,
		justifyContent: 'center',
		paddingHorizontal: moderateScale(40)
	},
	formContainerCenter: {
		position: 'relative'
	},
	logoContainer: {
		alignItems: 'center'
	},
	logoSize:{
		width: moderateScale(100),
		height: moderateScale(100),
		marginBottom: moderateScale(30)
	},
	loginButton: {
		alignItems: 'center',
		justifyContent: 'center',
		height: moderateScale(35),
		backgroundColor: '#08b2a9',
		marginTop: moderateScale(10),
		borderRadius: moderateScale(5)
	},
	registerButton: {
		alignItems: 'center',
		justifyContent: 'center',
		height: moderateScale(35),
		backgroundColor: '#f68421',
		marginTop: moderateScale(10),
		borderRadius: moderateScale(5)
	},
	loginButtonText: {
		color: '#ffffff',
		fontWeight: "700"
	},
	forgotPasswordText: {
		color: '#ffffff',
		fontSize: moderateScale(12),
		marginTop: moderateScale(-10),
		paddingStart: moderateScale(5)
	},
	vendorText: {
		color: '#ffffff',
		fontWeight: 'bold',
		fontSize: moderateScale(20),
		marginTop: moderateScale(20)
	}
});

export default Login
